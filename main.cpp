#include <wx/app.h>
#include <wx/button.h>
#include <wx/frame.h>
#include <wx/notebook.h> 
#include <wx/panel.h>
#include <wx/sizer.h>

class WindowWithTwoSubWindows : public wxWindow
{
public:

    WindowWithTwoSubWindows(wxWindow* Parent, wxWindowID Id = wxID_ANY) :
        wxWindow(Parent, Id)
    {
        auto LeftWindow = new wxWindow(this, wxID_ANY);
        auto RightWindow = new wxWindow(this, wxID_ANY);
        LeftWindow->SetBackgroundColour(*wxRED);
        RightWindow->SetBackgroundColour(*wxBLUE);
        auto RootSizer = new wxBoxSizer(wxHORIZONTAL);
        RootSizer->Add(LeftWindow, 1, wxEXPAND, 0);
        RootSizer->Add(RightWindow, 1, wxEXPAND, 0);
        /* Using buttons instead of the windows above
        would correctly split the window in half: */
        // RootSizer->Add(new wxButton(this, wxID_ANY, "LEFT"), 1, wxEXPAND, 0);
        // RootSizer->Add(new wxButton(this, wxID_ANY, "RIGH"), 1, wxEXPAND, 0);
        SetSizer(RootSizer);
        Layout(); 
    }
};

class MainFrame : public wxFrame
{
public:

    MainFrame() : 
        wxFrame(nullptr, wxID_ANY, "wxNotebookSizeProblem", wxDefaultPosition, {320, 240})
    {
        auto Notebook = new wxNotebook(this, wxID_ANY);
            auto WindowInside = new WindowWithTwoSubWindows(Notebook);
            WindowInside->SetBackgroundColour(*wxGREEN);
            Notebook->AddPage(WindowInside, "Notebook Page");

        auto RootSizer = new wxBoxSizer(wxHORIZONTAL);
        RootSizer->Add(Notebook, 1, wxEXPAND, 0);
        SetSizer(RootSizer);
        Layout(); 
    }
};

class App : public wxApp
{
    MainFrame* fFrame; 

public:

    virtual bool OnInit() override
    {
        fFrame = new MainFrame();
        fFrame->Show(true);
        return true;
    }
};

// construct the app:
IMPLEMENT_APP(App)
