# wxNotebookSizeProblem

The problem I have is that wxWindow instances have a faulty size of 20x20 when created inside a wxNotebook.
Thus, the MainFrame must be resized by a user at least once, before the contents of the notebook could be displayed with the correct size.

To demonstrate this, a simple app creates a frame instance:

```cpp
class MainFrame : public wxFrame
{
public:

    MainFrame() : wxFrame(nullptr, wxID_ANY, "wxNotebookSizeProblem", \
            wxDefaultPosition, {320, 240})
    {
        auto Notebook = new wxNotebook(this, wxID_ANY);
            auto WindowInside = new WindowWithTwoSubWindows(Notebook);
            WindowInside->SetBackgroundColour(*wxGREEN);
            Notebook->AddPage(WindowInside, "Notebook Page");

        auto RootSizer = new wxBoxSizer(wxHORIZONTAL);
        RootSizer->Add(Notebook, 1, wxEXPAND, 0);
        SetSizer(RootSizer);
        Layout();
    }
};
```

Inside the frame is a notebook, which is filled with a single `WindowWithTwoSubWindows` page.
The `WindowInside` instance has a green background, to show the faulty sizing.
It's class has the following definition:

```cpp
class WindowWithTwoSubWindows : public wxWindow
{
public:

    WindowWithTwoSubWindows(wxWindow* Parent, wxWindowID Id = wxID_ANY) :
        wxWindow(Parent, Id)
    {
        auto LeftWindow = new wxWindow(this, wxID_ANY);
        auto RightWindow = new wxWindow(this, wxID_ANY);
        LeftWindow->SetBackgroundColour(*wxRED);
        RightWindow->SetBackgroundColour(*wxBLUE);
        auto RootSizer = new wxBoxSizer(wxHORIZONTAL);
        RootSizer->Add(LeftWindow, 1, wxEXPAND, 0);
        RootSizer->Add(RightWindow, 1, wxEXPAND, 0);
        SetSizer(RootSizer);
        Layout();
    }
};
```

It is simply made up of two subwindows and a sizer.
Both of the subwindows have their own background colour to distinguish them easily.
They should share the horizontal space equally, according to `RootSizer`.
However, after the initial construction this is not the case, as they only share a 20x20 pixels area in the top left corner.
As soon as the main frame is resized by the user, the subwindows snap to their correct size (and behave as desired for the rest of the runtime).

![InitialSize.png](./Screenshots/InitialSize.png "Initial size")
![AfterResize.png](./Screenshots/AfterResize.png "After resizing")

Now, if buttons where used instead of the wxWindow instances, the problem disappears:

```cpp
class WindowWithTwoSubWindows : public wxWindow
{
public:

    WindowWithTwoSubWindows(wxWindow* Parent, wxWindowID Id = wxID_ANY) :
        wxWindow(Parent, Id)
    {
        auto RootSizer = new wxBoxSizer(wxHORIZONTAL);
        RootSizer->Add(new wxButton(this, wxID_ANY, "LEFT"), 1, wxEXPAND, 0);
        RootSizer->Add(new wxButton(this, wxID_ANY, "RIGH"), 1, wxEXPAND, 0);
        SetSizer(RootSizer);
        Layout();
    }
};
```

![Buttons.png](./Screenshots/Buttons.png "Using buttons instead of windows")

The green background is now only slightly visible between the buttons, which is the intended behaviour.

This has been observed with the following environments

```bash
> cat /etc/redhat-release
    Fedora release 29 (Twenty Nine)

> wx-config --list
    Default config is gtk3-unicode-3.0
```

and

```bash
> cat /etc/redhat-release
    CentOS Linux release 7.6.1810 (Core)

> wx-config-3.0 --list
    Default config is gtk3-unicode-3.0
```

The issue did *not* appear with this environment

```bash
> cat /proc/version
    Linux version 4.15.0-48-generic (buildd@lgw01-amd64-036) (gcc version 7.3.0 (Ubuntu 7.3.0-16ubuntu3)) #51-Ubuntu SMP Wed Apr 3 08:28:49 UTC 2019

> wx-config --list
    Default config is gtk2-unicode-3.0
```

Things I did try, which were not helpful:

1. [This post](https://forums.wxwidgets.org/viewtopic.php?f=1&t=30405&p=130525&hilit=wxNotebook+sizer#p130525) made me realize that the issue is not present for buttons.
2. Another, [similar post](https://forums.wxwidgets.org/viewtopic.php?f=1&t=29204&p=125135&hilit=wxNotebook#p125135) asked the same question, however with no conclusion.
3. Somewhere (can't remember where), I've found that it is not good style to place controls directly into a frame.
However, creating a panel, then adding the notebook with the panel as parent and changing the `SetSizer()` call to `panel->SetSizer()` does not solve this issue.
4. Switching `wxWindow` for `wxPanel` or `wxControl` did not change anything.
5. Calling `Layout()` on various locations, in various orders and on various controls did not change anything.
6. [This solution](https://forums.wxwidgets.org/viewtopic.php?p=10373#p10373) does not work.
